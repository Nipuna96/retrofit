package com.example.task1_refrofit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Adapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private Adaptery adaptery;
    RecyclerView recyclerView;
    List<DataModel> dataModelList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerView);
        dataModelList = new ArrayList<>();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://www.metaweather.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        MyAPICall myAPICall = retrofit.create(MyAPICall.class);
       Call<List<DataModel>> call = myAPICall.getData();

       call.enqueue(new Callback<List<DataModel>>() {
           @Override
           public void onResponse(Call<List<DataModel>> call, Response<List<DataModel>> response) {
               if(response.code() != 200){
                   return;
               }

               List<DataModel> allData =  response.body();

               for(DataModel data : allData){

                    dataModelList.add(data);
               }
               PutDataIntoRecyclerView(dataModelList);
           }

           @Override
           public void onFailure(Call<List<DataModel>> call, Throwable t) {

           }
       });
    }

    private void PutDataIntoRecyclerView(List<DataModel> dataModelList) {

        adaptery = new Adaptery(this, dataModelList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adaptery);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.data_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adaptery.getFilter().filter(newText);
                return false;
            }
        });
        return true;
    }
}
