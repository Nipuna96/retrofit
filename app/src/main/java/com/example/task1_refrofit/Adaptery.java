package com.example.task1_refrofit;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class Adaptery extends RecyclerView.Adapter<Adaptery.MyViewHolder> implements Filterable {

    private Context mContext;
    private List<DataModel> dataList;
    private List<DataModel> dataListCopy;

    public Adaptery(Context mContext, List<DataModel> dataList) {
        this.mContext = mContext;
        this.dataList = dataList;
        dataListCopy = new ArrayList<>(dataList);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        v = layoutInflater.inflate(R.layout.data_item, parent, false);
        return new MyViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.title.setText(dataList.get(position).getTitle());
        holder.location_type.setText(dataList.get(position).getLocation_type());
        holder.woeid.setText(dataList.get(position).getWoeid());
        holder.latt_long.setText(dataList.get(position).getLatt_long());
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder{

        TextView title;
        TextView location_type;
        TextView woeid;
        TextView latt_long;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.title);
            location_type = itemView.findViewById(R.id.location_type);
            woeid = itemView.findViewById(R.id.woeid);
            latt_long = itemView.findViewById(R.id.latt_long);
        }
    }

    @Override
    public Filter getFilter() {
        return dataFilter;
    }

    private Filter dataFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<DataModel> filteredList = new ArrayList<>();
            if(constraint == null || constraint.length() == 0){
                filteredList.addAll(dataListCopy);
            }else{
                String filterPattern = constraint.toString().toLowerCase().trim();
                for(DataModel item: dataListCopy){
                    if(item.getTitle().toLowerCase().startsWith(filterPattern)){
                        filteredList.add(item);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            dataList.clear();
            dataList.addAll((List)results.values);
            notifyDataSetChanged();
        }
    };
}
