package com.example.task1_refrofit;

public class DataModel {

    private String title;

    private String location_type;

    private String woeid;

    private String latt_long;

    public String getTitle() {
        return title;
    }

    public String getLocation_type() {
        return location_type;
    }

    public String getWoeid() {
        return woeid;
    }

    public String getLatt_long() {
        return latt_long;
    }
}

