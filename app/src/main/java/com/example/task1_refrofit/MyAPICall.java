package com.example.task1_refrofit;

import java.util.List;

import retrofit2.Call;

import retrofit2.http.GET;

public interface MyAPICall {

    @GET("api/location/search/?query=ba")
    Call<List<DataModel>> getData();


}
